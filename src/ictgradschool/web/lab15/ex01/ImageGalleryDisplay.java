package ictgradschool.web.lab15.ex01;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StreamCorruptedException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by ycow194 on 17/05/2017.
 */
public class ImageGalleryDisplay extends HttpServlet {

    public ImageGalleryDisplay() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        displayHTML(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }


    private void displayHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html>\n<head><title>Server response</title>");

        String fullPhotoPath = getServletContext().getRealPath("/Photos");
        java.io.File file = new java.io.File(fullPhotoPath);

        File[] listOfFiles = file.listFiles();


        int filenumber = file.listFiles().length;

        for (int i = 0; i < filenumber; i++) {
            if (listOfFiles[i].getName().contains(".png")) {
                out.print("<a href=" + "\\Photos\\" + listOfFiles[i-1].getName() + ">");
                out.print("<img src=");
                out.print("\\Photos\\" + listOfFiles[i].getName());
                out.print(" />");
                out.print("</a>");

                out.println("<p>Name: " + listOfFiles[i].getName() + "</p>");
                out.println("<p>File Size: " + listOfFiles[i].length() + "bytes</p>");
                out.println("<br>");
            }

        }

        out.println("</body></html>");
    }

}
//<a href=" + "\\Photos\\" + listOfFiles[i-1].getName() + ">